#coding: utf-8
import os
from django.utils.translation import ugettext_lazy as _
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SECRET_KEY = 'lqkytdvyg+obnv)q2os_vzphli6%hjt2swaabs!+-0#v&0qoz='
DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []
SITE_ID=1

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'ckeditor',
    #'filer',
    'django_select2',
    'rest_framework',


    'base',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
)

ROOT_URLCONF = 'indevgroup.urls'

WSGI_APPLICATION = 'indevgroup.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

LANGUAGE_CODE = 'ru'
LANGUAGES = (
                ('ru', _('Russian')),
                ('en', _('English')),
)
LOCALE_PATHS = (
    os.path.join(BASE_DIR, "locale"),
)
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
CKEDITOR_UPLOAD_PATH = 'images/'
FILEBROWSER_DIRECTORY = CKEDITOR_UPLOAD_PATH

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)
if DEBUG:
    TEMPLATE_CONTEXT_PROCESSORS+= ('django.core.context_processors.debug',)

SUIT_CONFIG = {
    'ADMIN_NAME': _('indevgroup'),
    'CONFIRM_UNSAVED_CHANGES': True,
    'MENU_EXCLUDE': ('auth', 'sites'),
    'MENU': (
        {'app': 'base', 'label': _(u'Общее'),},
    ),
}

CKEDITOR_TOOLBAR = (
    [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ],
    [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','aRedo' ],
    [ 'Find','Replace','-','SelectAll','-','SpellChecker'],
    [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
    [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidaiRtl' ],
    [ 'Link','Unlink','Anachor' ],
    [ 'Image', 'Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBareak' ],
    #[ 'Styles','Format','Font','FontaSize' ],
    #[ 'TextColor','BGColor' ],
    [ 'Maximize', 'ShowBlocks'],
)

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': CKEDITOR_TOOLBAR,
        'height': 400,
        'width': 'auto',
        'disableNativeSpellChecker': False,
        'forcePasteAsPlainText': True,
        'startupOutlineBlocks': True,
    },
}

try:
    from local_settings import *
except ImportError:
    pass