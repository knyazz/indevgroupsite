# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FlatPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ru_title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a (\u0440\u0443\u0441.)')),
                ('en_title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a (\u0430\u043d\u0433\u043b.)', blank=True)),
                ('cn_title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a (\u043a\u0438\u0442.)', blank=True)),
                ('ru_content', ckeditor.fields.RichTextField(default='<p>\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0435\u0449\u0451 \u043d\u0435 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u043e</p>', verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435 (\u0440\u0443\u0441.)')),
                ('ru_status', models.PositiveSmallIntegerField(default=2, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438 (\u0440\u0443\u0441.)', choices=[(0, '\u043d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u043e'), (1, '\u0447\u0435\u0440\u043d\u043e\u0432\u0438\u043a'), (2, '\u043e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e')])),
                ('en_content', ckeditor.fields.RichTextField(default='<p>No description</p>', verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435 (\u0430\u043d\u0433\u043b.)')),
                ('en_status', models.PositiveSmallIntegerField(default=2, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438 (\u0430\u043d\u0433\u043b.)', choices=[(0, '\u043d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u043e'), (1, '\u0447\u0435\u0440\u043d\u043e\u0432\u0438\u043a'), (2, '\u043e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e')])),
                ('cn_content', ckeditor.fields.RichTextField(default='<p>\u53d1\u5c55\u4e2d</p>', verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0430\u043d\u0438\u0435 (\u043a\u0438\u0442.)')),
                ('cn_status', models.PositiveSmallIntegerField(default=2, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438 (\u043a\u0438\u0442.)', choices=[(0, '\u043d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u043e'), (1, '\u0447\u0435\u0440\u043d\u043e\u0432\u0438\u043a'), (2, '\u043e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e')])),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('slug', models.SlugField(help_text='\u0417\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043d\u0430 \u0440\u0443\u0441\u0441\u043a\u043e\u043c \u044f\u0437\u044b\u043a\u0435', unique=True, max_length=255, verbose_name='\u041f\u0443\u0442\u044c')),
                ('ru_keywords', models.CharField(max_length=2048, verbose_name='SEO keywords', blank=True)),
                ('ru_description', models.CharField(max_length=2048, verbose_name='SEO description', blank=True)),
                ('en_keywords', models.CharField(max_length=2048, verbose_name='SEO keywords', blank=True)),
                ('en_description', models.CharField(max_length=2048, verbose_name='SEO description', blank=True)),
                ('cn_keywords', models.CharField(max_length=2048, verbose_name='SEO keywords', blank=True)),
                ('cn_description', models.CharField(max_length=2048, verbose_name='SEO description', blank=True)),
            ],
            options={
                'verbose_name': '\u043f\u0440\u043e\u0441\u0442\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u043f\u0440\u043e\u0441\u0442\u044b\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
            bases=(models.Model,),
        ),
    ]
