# coding: utf-8
from django.db import models


class DefaultQuerySet(models.query.QuerySet):
    def active_pages(self,request=None):
        if request:
            filt = {
                    'ru': { 'ru_title__isnull': False,
                            'ru_status': 2},
                    'en': { 'en_title__isnull': False,
                            'en_status': 2},
                    'zh-cn': {  'cn_title__isnull': False,
                                'cn_status': 2},
            }.get(request.LANGUAGE_CODE)
            return self.filter(models.Q(**filt))
        else:
            return self


class DefaultManager(models.Manager):
    def get_queryset(self):
        return DefaultQuerySet(self.model, using=self._db)

    def active_pages(self, request=None):
        return self.get_queryset().active_pages(request)