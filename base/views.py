# coding: utf-8
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views.generic import DetailView
from django.views.generic import TemplateView

from .models import FlatPage


class TempIndexPage(TemplateView):
    u''' Главная страница '''
    template_name = 'index.html'
temp_index = TempIndexPage.as_view()


class DetailPage(DetailView):
    template_name = 'object_detail.html'
    context_object_name = 'detail_page'
    model = FlatPage

    def get(self, request, *args, **kwargs):
        if self.get_object() in self.get_queryset().active_pages(request):
            return super(DetailPage, self).get(request, *args, **kwargs)
        else:
            return redirect(reverse('base:index'))
flat_page = DetailPage.as_view()


class IndexPage(DetailPage):
    def get_object(self, queryset=None):
        return self.model.objects.filter(slug='glavnaya').last()

    def get(self, request, *args, **kwargs):
        if self.get_object() in self.get_queryset().active_pages(request):
            return super(IndexPage, self).get(request, *args, **kwargs)
        else:
            return redirect(reverse('base:temp_index'))
index = IndexPage.as_view()