from django.conf.urls import patterns, url

urlpatterns = patterns('base.views',
    url(r'^$', 'index', name='index'),
    url(r'^temp_index/$', 'temp_index', name='temp_index'),
    url(r'^flatpages/(?P<slug>.*)/$', 'flat_page', name='flat_page'),
)