(function($, ymaps) {
  ymaps.ready(initMap);

  function initMap() {
    $('#mappart').html('');
    var mylocation = [59.9206237454545, 30.303342008329906];
    var geolocation = ymaps.geolocation,
      myMap = new ymaps.Map('mappart', {
        center: mylocation,
        zoom: 15,
        controls: ['zoomControl', 'fullscreenControl'],
      });

    myMap.behaviors.disable(['scrollZoom']);

    // Создание макета содержимого хинта.
    HintLayout = ymaps.templateLayoutFactory.createClass(
      "<div class='my-hint'>" +
      "<b>БЦ Аларчин мост</b><br />" +
      "Санкт-Петербург, проспект Римского-Корсакова, 73, офис 311б" +
      "</div>", {
        getShape: function() {
          var el = this.getElement(),
            result = null;
          if (el) {
            var firstChild = el.firstChild;
            result = new ymaps.shape.Rectangle(
              new ymaps.geometry.pixel.Rectangle([
                [0, 0],
                [firstChild.offsetWidth, firstChild.offsetHeight]
              ])
            );
          }
          return result;
        }
      }
    );

    myPlacemark = new ymaps.Placemark([59.9208, 30.2865], {
      address: "Санкт-Петербург, проспект Римского-Корсакова, 73, офис 311б",
      object: "БЦ Аларчин мост"
    }, {
      hintLayout: HintLayout,
      iconLayout: 'default#image',
      iconImageHref: '/static/images/logo/mapIcon_45x60.png',
      iconImageSize: [45, 60],
      iconImageOffset: [-38, -60]
    });

    myMap.geoObjects.add(myPlacemark);
  }



  $(function() {

    //слайдер
    $('#partners').owlCarousel({
      navigation: true, // Show next and prev buttons
      slideSpeed: 300,
      paginationSpeed: 400,
      pagination: true,
      singleItem: true,
      stopOnHover: true,
      navigationText: ["<a class='prevSlide'>&lt;</a>",
        "<a class='nextSlide'>&gt;</a>"
      ]
    });


    var controller = new ScrollMagic();

    controller.scrollTo(function(newpos) {
      var offset = 85;

      TweenMax.to(window, 0.5, {
        scrollTo: {
          y: newpos - offset
        }
      });
    });

    var $header = $(".header");

    //сжатие "шапки" при скроле вниз
    var tween = TweenMax.to(".header", 1, {
      css: {
        "padding-top": "0px",
        "padding-bottom": "0px"
      }
    });

    var scene = new ScrollScene({
        duration: 200,
        offset: 5
      })
      .setTween(tween)
      .addTo(controller);

    // scene.on("progress", function() {
    //   console.log('header resize', $header.innerHeight());
    // });

    //паравозик из направлений деятельности
    // var tween2 = TweenMax.staggerFromTo(".vectorcol", 1.5, {
    //   left: 1000
    // }, {
    //   left: 0
    // }, 4);
    //
    // var scene2 = new ScrollScene({
    //     triggerElement: "#section2",
    //     duration: 230
    //   })
    //   .setTween(tween2)
    //   .addTo(controller);


    //"взрыв" внизу направлений деятельности
    // var tween3 = TweenMax.fromTo(".vectorbottom", 1, {
    //   scale: 0.1
    // }, {
    //   scale: 1
    // });
    //
    // var scene3 = new ScrollScene({
    //     triggerElement: "#section2",
    //     triggerHook: 0.5,
    //     offset: 200
    //   })
    //   .setTween(tween3)
    //   .addTo(controller);


    // скрол при щелчке по якорю
    $(document).on("click", "a[href^=#]", function(e) {
      var id = $(this).attr("href");
      if ($(id).length > 0) {
        e.preventDefault();

        controller.scrollTo(id);

        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }
    });


  });

})(jQuery, ymaps);