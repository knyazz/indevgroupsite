# coding: utf-8
from django.db import models
from django.core.urlresolvers import reverse

from ckeditor.fields import RichTextField, RichTextFormField

from .choices import PUBLISH_CHOICES
from .managers import DefaultManager


class TitlePage(models.Model):
    ru_title = models.CharField(u'Заголовок (рус.)',
                                max_length=255,)
    en_title = models.CharField(u'Заголовок (англ.)',
                                max_length=255, blank=True)
    cn_title = models.CharField(u'Заголовок (кит.)',
                                max_length=255, blank=True)
    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title() or unicode(self.pk)

    def title(self, request=None):
        if request:
            return {
                    'ru': self.ru_title,
                    'en': self.en_title,
                    'zh-cn': self.cn_title,
            }.get(request.LANGUAGE_CODE)
        return self.ru_title


class MyRichTextFormField(RichTextFormField):
    def __init__(self, config_name='default', extra_plugins=None, external_plugin_resources=None, *args, **kwargs):
        kwargs.pop('max_length')
        super(MyRichTextFormField, self).__init__(*args, **kwargs)

class MyRichTextField(RichTextField):
    def formfield(self, **kwargs):
        defaults = {
            'form_class': MyRichTextFormField,
            'config_name': self.config_name,
            'extra_plugins' : self.extra_plugins,
            'external_plugin_resources': self.external_plugin_resources
        }
        defaults.update(kwargs)
        return super(MyRichTextField, self).formfield(**defaults)


class SimpleAbstractPage(TitlePage):
    ru_content = MyRichTextField(u'Содержание (рус.)',
                                default=u'''<p>Описание ещё не добавлено</p>''')
    ru_status = models.PositiveSmallIntegerField(u'Статус публикации (рус.)',
        default=2, choices=PUBLISH_CHOICES)
    en_content = MyRichTextField(u'Содержание (англ.)',
                                default=u'''<p>No description</p>''')
    en_status = models.PositiveSmallIntegerField(u'Статус публикации (англ.)',
        default=2, choices=PUBLISH_CHOICES)
    cn_content = MyRichTextField(u'Содержание (кит.)',
                                default=u'''<p>发展中</p>''')
    cn_status = models.PositiveSmallIntegerField(u'Статус публикации (кит.)',
        default=2, choices=PUBLISH_CHOICES)

    class Meta:
        abstract = True

    def content(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_content,
                    'en': self.en_content,
                    'zh-cn': self.cn_content,
            }.get(request.LANGUAGE_CODE)
        return self.ru_content

    def publish_status(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_status,
                    'en': self.en_status,
                    'zh-cn': self.cn_status,
            }.get(request.LANGUAGE_CODE)
        return self.ru_status


class AbstractPage(SimpleAbstractPage):
    objects = DefaultManager()
    created = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(u'Путь', max_length=255, unique=True,
                help_text=u'Заполнить заголовок на русском языке'
    )

    class Meta:
        ordering = ('-created',)
        abstract = True


class FlatPage(AbstractPage):
    u''' простая страница '''
    ru_keywords = models.CharField(u'SEO keywords',
                                max_length=2048, blank=True)
    ru_description = models.CharField(u'SEO description',
                                max_length=2048, blank=True)
    en_keywords = models.CharField(u'SEO keywords',
                                max_length=2048, blank=True)
    en_description = models.CharField(u'SEO description',
                                max_length=2048, blank=True)
    cn_keywords = models.CharField(u'SEO keywords',
                                max_length=2048, blank=True)
    cn_description = models.CharField(u'SEO description',
                                max_length=2048, blank=True)

    def keywords(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_keywords,
                    'en': self.en_keywords,
                    'zh-cn': self.cn_keywords,
            }.get(request.LANGUAGE_CODE)
        return self.ru_keywords

    def description(self, request=None):
        if request and request.LANGUAGE_CODE:
            return {
                    'ru': self.ru_description,
                    'en': self.en_description,
                    'zh-cn': self.cn_description,
            }.get(request.LANGUAGE_CODE)
        return self.ru_description

    class Meta:
        verbose_name= u'простая страница'
        verbose_name_plural = u'простые страницы'

    def get_absolute_url(self):
        return reverse('base:flat_page', kwargs=dict(slug=self.slug))

    def get_seo(self, request=None):
        return self.title(request) or self.keywords(request) or self.description(request)