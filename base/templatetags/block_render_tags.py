#coding: utf-8
from __future__ import unicode_literals

from django import template

register = template.Library()


@register.simple_tag
def render_locale_title(request, page):
    return page and hasattr(page, 'title') and page.title(request) or ''


@register.simple_tag
def render_locale_content(request, page):
    return page and hasattr(page, 'content') and page.content(request) or ''


@register.simple_tag
def render_locale_field(request, page, field):
    return page and hasattr(page, field) and getattr(page, field)(request) or ''