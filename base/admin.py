#coding:  utf-8
from django import forms
from django.conf import settings
from django.contrib import admin
from django.db import models

from suit.widgets import LinkedSelect, SuitDateWidget

from .models import FlatPage


DEFAULT_FIELDSETS = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ('slug',)
        }),
        # русский
        (None, {
            'classes': ('suit-tab suit-tab-ru',),
            'fields': ('ru_title', 'ru_status')
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-ru'),
            'fields': ('ru_content',)
        }),
        #английский 
        (None, {
            'classes': ('suit-tab suit-tab-en',),
            'fields': ('en_title', 'en_status')
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-en'),
            'fields': ('en_content',)
        }),
        #китайский
        (None, {
            'classes': ('suit-tab suit-tab-zh-cn',),
            'fields': ('cn_title', 'cn_status')
        }),
        (None, {
            'classes': ('full-width', 'suit-tab suit-tab-zh-cn'),
            'fields': ('cn_content',)
        }),
    )

DEFAULT_FORMTABS = (('general', u'Общее',),)+settings.LANGUAGES


class DefaultForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DefaultForm, self).__init__(*args, **kwargs)
        for k,v in self.fields.items():
            if self.fields[k].required:
                v.widget.attrs['required']='required'



class AdminBase(admin.ModelAdmin):
    form = DefaultForm
    formfield_overrides = {
        models.DateField: {'widget': SuitDateWidget},
        models.ForeignKey: {'widget': LinkedSelect},
        models.OneToOneField: {'widget': LinkedSelect},
    }
    class Media:
        js = (
                'js/admin.js',
        )


class SimplePageAdmin(AdminBase):
    fieldsets = DEFAULT_FIELDSETS
    suit_form_tabs = DEFAULT_FORMTABS


class FlatPageAdmin(SimplePageAdmin):
    prepopulated_fields = { 'en_title': ('ru_title',),
                            'cn_title': ('ru_title',),
                            'slug': ('ru_title',)
                        }

class FlatPageWithSeo(FlatPageAdmin):
    def __init__(self, *args, **kwargs):
        super(FlatPageWithSeo, self).__init__(*args, **kwargs)
        self.fieldsets+= (
            ('SEO', {
                'classes': ('suit-tab suit-tab-ru',),
                'fields': ('ru_keywords', 'ru_description')
            }),
            ('SEO', {
                'classes': ('suit-tab suit-tab-en',),
                'fields': ('en_keywords', 'en_description')
            }),
            ('SEO', {
                'classes': ('suit-tab suit-tab-cn',),
                'fields': ('cn_keywords', 'cn_description')
            }),
        )
admin.site.register(FlatPage, FlatPageWithSeo)